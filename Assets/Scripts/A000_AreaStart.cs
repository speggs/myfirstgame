﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class A000_AreaStart : MonoBehaviour {

	public GameObject BlackScreen;
	public GameObject FadeScreen;

	void Start () {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		StartCoroutine(BlackScreenDown(1));
		StartCoroutine(FadeScreenDown(1.98f));
	}
	
	IEnumerator BlackScreenDown(float time){
		yield return new WaitForSeconds(time);
		BlackScreen.SetActive(false);
		FadeScreen.GetComponent<Animator>().enabled = true;
	}
	
	IEnumerator FadeScreenDown(float time){
		yield return new WaitForSeconds(time);
		FadeScreen.GetComponent<Animator>().enabled = false;
		FadeScreen.SetActive(false);
	}
}
