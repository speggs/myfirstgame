﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warp : MonoBehaviour {
	public Transform warptarget;
	
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter(Collider col){
		if(col.gameObject.tag == "warp001"){
			this.transform.position = warptarget.position;
		}
	}
}
