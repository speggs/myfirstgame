﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalCash : MonoBehaviour {
	public static int CurrentCoins = 100;
	public int LocalCoins;
	public GameObject InventoryDisplay;
	public GameObject ShopDisplay;
	
	void Update(){
		LocalCoins = CurrentCoins;
		InventoryDisplay.GetComponent<Text>().text = "Coins: " + LocalCoins;
		ShopDisplay.GetComponent<Text>().text = "Coins: " + LocalCoins;
	}
}
