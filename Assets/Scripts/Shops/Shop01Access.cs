﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Shop01Access : MonoBehaviour {

	public GameObject ShopInventory;
	
	public GameObject Item01Text;
	public GameObject Item02Text;
	public GameObject Item03Text;
	public GameObject Item04Text;
	
	public GameObject Item01PriceBox;
	public GameObject Item02PriceBox;
	public GameObject Item03PriceBox;
	public GameObject Item04PriceBox;
	
	public GameObject ItemCompletion;
	public GameObject CompleteText;
	public GameObject ThePlayer;
	
	public int ItemPurchaseNumber;
	public GameObject NotEnough;
	
	void OnMouseDown() {
		ThePlayer.GetComponent<FirstPersonController>().enabled = false;
		ShopInventory.SetActive(true);
		Screen.lockCursor = false;
		// GlobalShop.ShopNumber = 1;
		GlobalShop.SetShopNumber(1);
		
		Item01Text.GetComponent<Text>().text = "" + GlobalShop.Item01;
		Item02Text.GetComponent<Text>().text = "" + GlobalShop.Item02;
		Item03Text.GetComponent<Text>().text = "" + GlobalShop.Item03;
		Item04Text.GetComponent<Text>().text = "" + GlobalShop.Item04;
		
		Item01PriceBox.GetComponent<Text>().text = "Price: " + GlobalShop.Item01Price;
		Item02PriceBox.GetComponent<Text>().text = "Price: " + GlobalShop.Item02Price;
		Item03PriceBox.GetComponent<Text>().text = "Price: " + GlobalShop.Item03Price;
		Item04PriceBox.GetComponent<Text>().text = "Price: " + GlobalShop.Item04Price;
	}
	
	public void Item01() {
		ItemCompletion.SetActive(true);
		CompleteText.GetComponent<Text>().text = "Confirm Purchase of " + GlobalShop.Item01 + "?";
		ItemPurchaseNumber = 1;
	}
	public void Item02() {
		ItemCompletion.SetActive(true);
		CompleteText.GetComponent<Text>().text = "Confirm Purchase of " + GlobalShop.Item02 + "?";
		ItemPurchaseNumber = 2;
	}
	public void Item03() {
		ItemCompletion.SetActive(true);
		CompleteText.GetComponent<Text>().text = "Confirm Purchase of " + GlobalShop.Item03 + "?";
		ItemPurchaseNumber = 3;
	}
	public void Item04() {
		ItemCompletion.SetActive(true);
		CompleteText.GetComponent<Text>().text = "Confirm Purchase of " + GlobalShop.Item04 + "?";
		ItemPurchaseNumber = 4;
	}
	
	public void CancelTransaction(){
		ItemCompletion.SetActive(false);
		ItemPurchaseNumber = 0;
		NotEnough.SetActive(false);
	}
	
	public void CompleteTransaction(){
		if(ItemPurchaseNumber == 1){
			if(GlobalCash.CurrentCoins >= GlobalShop.Item01Price){
				GlobalCash.CurrentCoins -= GlobalShop.Item01Price;
				ItemCompletion.SetActive(false);
			}else{
				NotEnough.SetActive(true);
			}
		}
		if(ItemPurchaseNumber == 2){
			if(GlobalCash.CurrentCoins >= GlobalShop.Item02Price){
				GlobalCash.CurrentCoins -= GlobalShop.Item02Price;
				ItemCompletion.SetActive(false);
			}else{
				NotEnough.SetActive(true);
			}
		}
		if(ItemPurchaseNumber == 3){
			if(GlobalCash.CurrentCoins >= GlobalShop.Item03Price){
				GlobalCash.CurrentCoins -= GlobalShop.Item03Price;
				ItemCompletion.SetActive(false);
			}else{
				NotEnough.SetActive(true);
			}
		}
		if(ItemPurchaseNumber == 4){
			if(GlobalCash.CurrentCoins >= GlobalShop.Item04Price){
				GlobalCash.CurrentCoins -= GlobalShop.Item04Price;
				ItemCompletion.SetActive(false);
			}else{
				NotEnough.SetActive(true);
			}
		}
	}
}
