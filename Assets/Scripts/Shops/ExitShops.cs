﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ExitShops : MonoBehaviour {
	public GameObject ThePlayer;
	public GameObject ShopPanel;
	public GameObject NotEnough;
	
	public void ExitShopMode() {
		ThePlayer.GetComponent<FirstPersonController>().enabled = true;
		ShopPanel.SetActive(false);
		Screen.lockCursor = true;
		NotEnough.SetActive(false);
		
	}
	
}
