﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntranceLever : MonoBehaviour {
	
	public GameObject TheLever;
	public AudioSource LeverSound;
	public GameObject TheLedge;
	
	void Update(){
		
	}
	
	void OnMouseDown(){
		GetComponent<BoxCollider>().enabled = false;
		TheLever.GetComponent<Animation>().Play("EntranceAnim");
		LeverSound.Play();
		TheLedge.GetComponent<Animator>().enabled = true;
	}
}
