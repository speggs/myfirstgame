﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveDoorOpen : MonoBehaviour {

	public GameObject DoorSwing;
	public GameObject TheKey;
	public AudioSource KeySound;
	public AudioSource CreakSound;
	public GameObject ThisObject;
	
	public void OnMouseDown(){
		Debug.Log("Cave Door Triggered");
		ThisObject.GetComponent<BoxCollider>().enabled = false;
		TheKey.SetActive(false);
		KeySound.Play();
		StartCoroutine(OpenDoor());
	}
	
	IEnumerator OpenDoor(){
		yield return new WaitForSeconds(1f);
		DoorSwing.GetComponent<Animator>().enabled = true;
		CreakSound.Play();
		yield return new WaitForSeconds(0.98f);
		DoorSwing.GetComponent<Animator>().enabled = false;
	}
}
