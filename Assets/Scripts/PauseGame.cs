﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseGame : MonoBehaviour {
	public GameObject ThePlayer;
	public bool Paused = false;
	public GameObject PauseMenu;
	
	void Update () {
		if(Input.GetButtonDown("Cancel")){
			if(Paused == false){
				PauseMenu.SetActive(true);
				Time.timeScale = 0;
				Paused = true;
				ThePlayer.GetComponent<FirstPersonController>().enabled = false;
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}else{
				ThePlayer.GetComponent<FirstPersonController>().enabled = true;
				Paused = false;
				Time.timeScale = 1;
				PauseMenu.SetActive(false);
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
	}
	
	public void UnpauseGame(){
		ThePlayer.GetComponent<FirstPersonController>().enabled = true;
		Paused = false;
		Time.timeScale = 1;
		PauseMenu.SetActive(false);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}
}
