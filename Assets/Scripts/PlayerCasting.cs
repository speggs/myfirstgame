﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCasting : MonoBehaviour {

	public static float DistanceFromTarget;
	
	void Update () {
		RaycastHit hit;
		if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit)){
			DistanceFromTarget = hit.distance;
			PlayerPrefs.SetFloat("DistanceFromTargetSave", DistanceFromTarget);
		}
	}
}
