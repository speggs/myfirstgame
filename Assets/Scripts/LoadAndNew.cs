﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadAndNew : MonoBehaviour {

	public string fileName = "savegame.data";
	public string LoadCode;
	public static string GlobalLoad;
	
	public GameObject NoSave;
	
	void Start () {
		StreamReader sr = new StreamReader(fileName);
		string line = sr.ReadLine();
		while(line != null){
			LoadCode = line;
			line = sr.ReadLine();
		}
		sr.Close();
		// GlobalLoad = LoadCode;
	}
	
	public void LoadGame(){
		GlobalLoad = LoadCode;
		if(GlobalLoad == "savearea001"){
			Application.LoadLevel(1);
		}else{
			NoSave.SetActive(true);
		}
	}
	
	public void QuitGame(){
		Application.Quit();
	}
	
	public void NewGame(){
		Application.LoadLevel(1);
	}
}
