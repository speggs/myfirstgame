﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollect : MonoBehaviour {

	IEnumerator OnTriggerEnter (Collider other) { 
		AudioSource coinaudio = GetComponent<AudioSource>();
		coinaudio.Play();
		CoinSystem.coinscollect += 1;
		transform.position = new Vector3(0, -1000, 0);
		yield return new WaitForSeconds(1.5f);
		Destroy (this.gameObject); 
	}﻿
}
