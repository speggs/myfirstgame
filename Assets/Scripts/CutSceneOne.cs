﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneOne : MonoBehaviour {
	public GameObject Cam1;
	public GameObject Cam2;
	public GameObject ThePlayer;
	public GameObject TheCanvas;
	
	void OnTriggerEnter(){
		StartCoroutine(CutSceneShow());
	}
	
	IEnumerator CutSceneShow(){
		TheCanvas.SetActive(false);
		Cam1.SetActive(true);
		ThePlayer.SetActive(false);
		yield return new WaitForSeconds(9.8f);
		// Cam2.SetActive(true);
		// Cam1.SetActive(false);
		// yield return new WaitForSeconds(4.8f);
		ThePlayer.SetActive(true);
		Cam2.SetActive(false);
		TheCanvas.SetActive(true);
		Destroy(gameObject);
	}
}
