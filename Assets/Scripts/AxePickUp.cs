﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AxePickUp : MonoBehaviour {
	
	public float TheDistance;
	public GameObject TextDisplay;
	public GameObject FakeAxe;
	public GameObject InventoryAxe;
	private bool canTakeAxe;
	
	void Update () {
			TheDistance = PlayerCasting.DistanceFromTarget;
	}
	
	void OnMouseOver(){
		if(TheDistance <= 10){
			// Debug.Log("Mouse Over Distance = " + TheDistance );
			TextDisplay.GetComponent<Text>().text = "Take Axe";
			canTakeAxe = true;
		}
	}
	
	void OnMouseExit() {
		// Debug.Log("Mouse Exit Distance = " + TheDistance );
		TextDisplay.GetComponent<Text>().text = "";
		canTakeAxe = false;
	}
	
	void OnMouseDown(){
		// Debug.Log("Mouse Down Distance = " + TheDistance );
		if(TheDistance <= 10 || canTakeAxe){
			transform.position = new Vector3(0, -1000, 0);
			InventoryAxe.SetActive(true);
			FakeAxe.SetActive(false);
		}
	}
}
