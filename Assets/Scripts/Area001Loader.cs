﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Area001Loader : MonoBehaviour {
	public GameObject ThePlayer;
	public GameObject StartScript;
	public GameObject VillageBox;
	public float PlayerX = 13.63f;
	public float PlayerY = 7.1f;
	public float PlayerZ = -11.57494f;
	public string LoadedCode;
	public GameObject QuestStatus;

	void Start () {
		LoadedCode = LoadAndNew.GlobalLoad;
		if(LoadedCode == "savearea001"){
			ThePlayer.transform.position = new Vector3(PlayerX, PlayerY, PlayerZ);
			StartScript.SetActive(false);
			VillageBox.SetActive(false);
			QuestStatus.GetComponent<Text>().text = "Active Quest: Reach The Village";
		}
	}
}
