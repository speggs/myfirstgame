﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeInfoPanel : MonoBehaviour {
	public GameObject InfoPanel;
	public GameObject TheAxe;
	public GameObject ItemEquipped;
	
	public void ItemEquip(){
		TheAxe.SetActive(true);
		ItemEquipped.SetActive(true);
		InfoPanel.SetActive(false);
	}
	
	public void ItemCancel(){
		InfoPanel.SetActive(false);
	}
}
