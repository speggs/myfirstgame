﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInfoPanel : MonoBehaviour {
	public GameObject InfoPanel;
	
	public void OpenPanel(){
		InfoPanel.SetActive(true);
	}
}
