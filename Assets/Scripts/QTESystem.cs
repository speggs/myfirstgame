﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTESystem : MonoBehaviour {
	public GameObject displayBox;
	public int QTEGen;
	public bool waitingForKey = true;
	public int correctKey;
	public GameObject passBox;
	
	void Update () {
		if(waitingForKey){
			waitingForKey = false;
			QTEGen = Random.Range(1, 3);
			if(QTEGen == 1){
				displayBox.GetComponent<Text>().text = "[R]";
			}else if(QTEGen == 2){
				displayBox.GetComponent<Text>().text = "[T]";
			}
		}
		
		if(QTEGen == 1){
			if(Input.anyKeyDown){
				if(Input.GetButtonDown("RKey")){
					correctKey = 1;
					StartCoroutine(KeyPressing());
				}else{
					correctKey = 2;
					StartCoroutine(KeyPressing());
				}
			}
		}else if(QTEGen == 2){
			if(Input.anyKeyDown){
				if(Input.GetButtonDown("TKey")){
					correctKey = 1;
					StartCoroutine(KeyPressing());
				}else{
					correctKey = 2;
					StartCoroutine(KeyPressing());
				}
			}
		}
	}
	
	IEnumerator KeyPressing(){
		QTEGen = 4;
		if(correctKey == 1){
			passBox.GetComponent<Text>().text = "Correct";
			yield return new WaitForSeconds(1.5f);
			correctKey = 0;
			passBox.GetComponent<Text>().text = "";
			displayBox.GetComponent<Text>().text = "";
			yield return new WaitForSeconds(1.5f);
			FirstQTE.timesDone += 1;
			waitingForKey = true;
		}
		if(correctKey == 2){
			passBox.GetComponent<Text>().text = "Incorrect";
			yield return new WaitForSeconds(1.5f);
			correctKey = 0;
			passBox.GetComponent<Text>().text = "";
			displayBox.GetComponent<Text>().text = "";
			yield return new WaitForSeconds(1.5f);
			waitingForKey = true;
		}
	}
}
