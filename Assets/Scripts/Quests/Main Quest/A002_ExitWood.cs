﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class A002_ExitWood : MonoBehaviour {

	public GameObject TheTextBox;
	public Text PlayerText;
	public GameObject QuestStatus;
	
	public AudioSource Line003;
	
	void OnTriggerEnter(Collider col){
		TheTextBox.SetActive(true);
		PlayerText.text = ("Looks like a village over that bridge");
		Line003.Play();
		StartCoroutine(ExitWoodQuest());
	}
	
	IEnumerator ExitWoodQuest(){
		yield return new WaitForSeconds(3f);
		
		PlayerText.text = "";
		TheTextBox.SetActive(false);
		yield return new WaitForSeconds(1f);
		
		TheTextBox.SetActive(true);
		PlayerText.text = "I should cross that bridge";
		yield return new WaitForSeconds(3f);
		
		PlayerText.text = "";
		TheTextBox.SetActive(false);
		QuestStatus.GetComponent<Text>().text = "Active Quest: Reach The Village";
		
		// this.transform.position = new Vector3(0, -1000, 0);
		this.gameObject.SetActive(false);
	}
}
