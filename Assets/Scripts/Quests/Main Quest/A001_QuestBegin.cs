﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using UnityEngine.UI;

public class A001_QuestBegin : MonoBehaviour {
	public GameObject QuestUpdate;
	public Text PlayerText;
	public GameObject TextDisplay;
	public int TimerCounter = 0;
	
	public AudioSource Line001;
	public AudioSource Line002;

	void Start () {
		QuestBegin001();
	}
	
	void QuestBegin001(){
		this.transform.position = new Vector3(0, -1000, 0);
		QuestUpdate.GetComponent<Text>().text = "Active Quest: Exit The Woods";
		
		if(TimerCounter == 0){
			StartCoroutine(Timer(1f));
		}
		if(TimerCounter == 1){
			TextDisplay.SetActive(true);
			Line001.Play();
			PlayerText.text = "Where am I?";
			StartCoroutine(Timer(2f));
		}
		if(TimerCounter == 2){
			Line002.Play();
			PlayerText.text = "I need to find a way out of these woods.";
			StartCoroutine(Timer(2f));
		}
		if(TimerCounter == 3){
			PlayerText.text = "";
			TextDisplay.SetActive(false);
			this.gameObject.SetActive(false);
			//StartCoroutine(Timer(1f));
		}
	}
	
	IEnumerator Timer(float time){
		yield return new WaitForSeconds(time);
		TimerCounter += 1;
		QuestBegin001();
	}
}
