﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest002End : MonoBehaviour {
	public int TextBoxOnCheck;
	public GameObject MessageBox;
	public GameObject TextBox;
	public GameObject QuestBox;
	public GameObject QuestText;
	
	public GameObject GoldBar;
	
	public void OnMouseDown(){
		if(TextBoxOnCheck == 0){
			TextBoxOnCheck = 1;
			MessageBox.SetActive(true);
			TextBox.GetComponent<Text>().text = "Servant: Thanks man.";
			QuestText.GetComponent<Text>().text = "Active Quest: None";
			GoldBar.SetActive(false);
		}else{
			TextBoxOnCheck = 0;
			MessageBox.SetActive(false);
		}
	}
	
		void Update () {
		if(Input.GetButtonDown("Submit")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
	}
}
