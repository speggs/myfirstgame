﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest002Gold : MonoBehaviour {
	public GameObject TheGold;
	public GameObject GoldPic;
	public GameObject ThisObject;
	public GameObject QuestActive;
	public GameObject QuestEnd;
	
	void OnMouseDown(){
		ThisObject.GetComponent<BoxCollider>().enabled = false;
		GoldPic.SetActive(true);
		TheGold.SetActive(false);
		QuestActive.SetActive(false);
		QuestEnd.SetActive(true);
		ThisObject.SetActive(false);
	}
}
