﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC002idle : MonoBehaviour {
	public int TextBoxOnCheck =0;
	public GameObject MessageBox;
	public GameObject TextBox;
	
	void OnMouseDown(){
		Debug.Log("On Mouse Down");
		if(TextBoxOnCheck == 0){;
			TextBoxOnCheck = 1;
			MessageBox.SetActive(true);
			TextBox.GetComponent<Text>().text = "Servant: Leave me alone!";
		}else{
			TextBoxOnCheck = 0;
			MessageBox.SetActive(false);
			//TextMessage = "Villager: Get going then!";
		}
	}

	void Update () {
		if(Input.GetButtonDown("Submit")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
	}
}
