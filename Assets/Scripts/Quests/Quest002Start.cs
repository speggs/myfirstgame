﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest002Start : MonoBehaviour {
	public int TextBoxOnCheck;
	public GameObject MessageBox;
	public GameObject TextBox;
	public GameObject QuestBox;
	public GameObject QuestText;
	public GameObject Key001;
	public GameObject CaveDoor;
	
	public void OnMouseDown(){
		if(TextBoxOnCheck == 0){
			TextBoxOnCheck = 1;
			MessageBox.SetActive(true);
			TextBox.GetComponent<Text>().text = "Servant: I want you to retrieve some Gold Bullion from the cave down the path. Here is the key.";
			QuestText.GetComponent<Text>().text = "Active Quest: Retrieve the Gold Bullion";
			Key001.SetActive(true);
			CaveDoor.SetActive(true);
		}else{
			TextBoxOnCheck = 0;
			MessageBox.SetActive(false);
		}
	}
	
		void Update () {
		if(Input.GetButtonDown("Submit")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
	}
}
