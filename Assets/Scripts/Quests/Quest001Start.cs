﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest001Start : MonoBehaviour {

	public int TextBoxOnCheck =0;
	public GameObject MessageBox;
	public GameObject TextBox;
	public string TextMessage;
	public GameObject QuestBox;
	public GameObject QuestText;
	public string QuestName;
	
	public GameObject QuestItemToShow;
	
	void OnMouseDown(){
		if(TextBoxOnCheck == 0){
			QuestItemToShow.SetActive(true);
			TextBoxOnCheck = 1;
			MessageBox.SetActive(true);
			TextBox.GetComponent<Text>().text = TextMessage;
			QuestName = "Active Quest: 'Recover The Loot'";
			//QuestBox.SetActive(true);
			QuestText.GetComponent<Text>().text = QuestName;
		}else{
			TextBoxOnCheck = 0;
			MessageBox.SetActive(false);
			TextMessage = "Villager: Get going then!";
		}
	}

	void Update () {
		if(Input.GetButtonDown("Submit")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
	}
}
