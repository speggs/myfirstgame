﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest001End : MonoBehaviour {

	public int TextBoxOnCheck =0;
	public GameObject MessageBox;
	public GameObject TextBox;
	public GameObject QuestBox;
	public GameObject QuestText;
	public GameObject NPC002Idle;
	public GameObject Quest002;
	
	void OnMouseDown(){
		Debug.Log("On Mouse Down");
		if(TextBoxOnCheck == 0){;
			TextBoxOnCheck = 1;
			MessageBox.SetActive(true);
			TextBox.GetComponent<Text>().text = "Villager: Thank You. Speak to the servant around the back.";
			QuestText.GetComponent<Text>().text = "Active Quest: 'Speak To Servant'";
			NPC002Idle.SetActive(false);
			Quest002.SetActive(true);
		}else{
			TextBoxOnCheck = 0;
			MessageBox.SetActive(false);
			//TextMessage = "Villager: Get going then!";
		}
	}

	void Update () {
		if(Input.GetButtonDown("Submit")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(TextBoxOnCheck == 1){
				MessageBox.SetActive(false);
				TextBoxOnCheck = 0;
			}
		}
	}
}
